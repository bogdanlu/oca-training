package Chapter2_OperatorsAndStatements;

public class IncrementAndDecrement {
    public static void main(String[] args){
        int counter = 0;
        System.out.println(counter); //outputs 0
        System.out.println(++counter); //outputs 1
        System.out.println(counter); //outputs 1
        System.out.println(counter--); //outputs 1
        System.out.println(counter); //outputs 0

    }
}
