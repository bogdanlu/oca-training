package Chapter1_JavaBuildingBlocks;

// Fields and instance initializer blocks are run in the order of appearance
// Constructor runs after fields and instance initializer blocks have run
public class OrderOfInitialization {
    // compile error, order matters
    //    {
    //        System.out.println(name);
    //    }

    private String name = "Luca";
    {
        name = "Bogdan";
        System.out.println(name);
    }

    public OrderOfInitialization() {
        name = "Cos";
        System.out.println(name);
    }

    public static void main(String[] args) {
        OrderOfInitialization example = new OrderOfInitialization();
        example.name = "Alex";
        System.out.println(example.name);
    }

    {
        name = "Dragos";
        System.out.println(name);
    }
}
