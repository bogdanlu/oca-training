package Chapter1_JavaBuildingBlocks;

public class Primitives {
    // compile error; integer too large
    //long longNr = 12345678910;
    long longNr = 12345678910L;
    int nrWith_ = 1_000_000;
    // underscore cannot be set at start or end of literal, or right before/after decimal point
}
