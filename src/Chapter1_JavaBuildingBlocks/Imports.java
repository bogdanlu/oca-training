package Chapter1_JavaBuildingBlocks;

import java.lang.*; // redundant because java.lang is always imported

// import java.nio.*.*; // incorrect because only one wildcard can be used
import java.nio.*; //wildcard matches only class names, not subpackages

// Naming conflicts
import java.util.*;
import java.sql.*; // Does not compile; The type Date is ambiguous
// -> fix
import java.util.Date; // takes precedence over wildcard
import java.sql.*;

import java.util.Date;
// import java.sql.Date; // Does not compile; import collision
// -> fix: use at most one as import and the other as fully qualified class name

public class Imports {
    Date date;

    public static void main(String[] args) {
        System.out.println("Imports and packages");
    }
}
