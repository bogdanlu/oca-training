package Chapter1_JavaBuildingBlocks;

public class Constructors {
    String name = "Name"; // initialize on line

    public Constructors() {
        System.out.println(name);
        name = "Luca"; // initialize in constructor
        System.out.println(name);
    }

    public void Constructors() {
        // not a constructor, just a method
        // pay attention during exam
    }

    public static void main(String[] args) {
        Constructors c = new Constructors();
        c.name = "Bogdan";
        System.out.println(c.name);
    }

    // compiler defines a 'do nothing' constructor
}
